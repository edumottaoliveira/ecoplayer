entities = [
  {
    name:'Cooperativa ReciclaMil', 
    accept: 'papel', 
    position: new google.maps.LatLng(-20.3003872,-40.3157217), 
    type: 'company', 
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Cooperativa ReciclaMil</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe papel e papelão.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Maria dos Santos', 
    accept: 'papel', 
    position: new google.maps.LatLng(-20.2953872,-40.2997217), 
    type: 'person',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Maria dos Santos</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe papel e papelão.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Jonas Abreu', 
    accept: 'aluminio', 
    position: new google.maps.LatLng(-20.3129942,-40.3235598), 
    type: 'person',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Jonas Abreu</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe latas de anlumínio.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Marcos Vieira', 
    accept: 'vidro', 
    position: new google.maps.LatLng(-20.3085642,-40.3248312), 
    type: 'person',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Marcos Vieira</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe garrafas de vidro.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Luana Rosa', 
    accept: 'vidro', 
    position: new google.maps.LatLng(-20.2977606,-40.324445), 
    type: 'person',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Luana Rosa</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe garrafas de vidro.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Jorge Oliveira', 
    accept: 'aluminio', 
    position: new google.maps.LatLng(-20.3133311,-40.3062444), 
    type: 'person',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Jorge Oliveira</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe latas de aluminio.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Adenilson Paes', 
    accept: 'aluminio', 
    position: new google.maps.LatLng(-20.31152,-40.3089266), 
    type: 'person',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Adenilson Paes</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe latas de aluminio.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  },
  {
    name:'Recicla Fácil', 
    accept: 'aluminio', 
    position: new google.maps.LatLng(-20.3320516,-40.2989148), 
    type: 'company',
    text:'<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h4 id="firstHeading" class="firstHeading">Recicla Fácil</h4>'+
    '<div id="bodyContent">'+
    '<p>Recebe latas de aluminio.</p>'+
    '<a href="">Detalhes</a> '+
    '</div>'+
    '</div>'
  }
];

var entitiesToShow = [];
var latlng;

function initGoogleMaps(position) {
  latlng = position;
  console.log(position);

  var myLatlng;
  if (position != null) {
    myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
  } else {
    myLatlng = new google.maps.LatLng(40.748817, -73.985428);
  }

  var mapOptions = {
    zoom: 14,
    center: myLatlng,
    scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
    styles: [{
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e9e9e9"
      }, {
        "lightness": 17
      }]
    }, {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "lightness": 20
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 17
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 29
      }, {
        "weight": 0.2
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 18
      }]
    }, {
      "featureType": "road.local",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }, {
        "lightness": 16
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "lightness": 21
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dedede"
      }, {
        "lightness": 21
      }]
    }, {
      "elementType": "labels.text.stroke",
      "stylers": [{
        "visibility": "on"
      }, {
        "color": "#ffffff"
      }, {
        "lightness": 16
      }]
    }, {
      "elementType": "labels.text.fill",
      "stylers": [{
        "saturation": 36
      }, {
        "color": "#333333"
      }, {
        "lightness": 40
      }]
    }, {
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f2f2f2"
      }, {
        "lightness": 19
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#fefefe"
      }, {
        "lightness": 20
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#fefefe"
      }, {
        "lightness": 17
      }, {
        "weight": 1.2
      }]
    }]
  };

  
  var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
  var icons = {
    company: {icon: iconBase + 'placemark_circle_highlight.png'},
    person: {icon: iconBase + 'placemark_circle.png'}
  };
  
  /*var features = [


    { position: new google.maps.LatLng(-20.3133914,-40.3029614), type: 'person'},

    { position: new google.maps.LatLng(-20.31152,-40.3089266), type: 'person'},
    { position: new google.maps.LatLng(-20.31052,-40.3089266), type: 'person'},
    { position: new google.maps.LatLng(-20.30552,-40.3074266), type: 'person'},
    { position: new google.maps.LatLng(-20.3133914,-40.3029614), type: 'person'}, 
    { position: new google.maps.LatLng(-20.1214396,-40.310), type: 'company'}, 
    { position: new google.maps.LatLng(-20.1514396,-40.320), type: 'company'}, 
    { position: new google.maps.LatLng(-20.31152,-40.3089266), type: 'company'}, 
    { position: new google.maps.LatLng(-20.3040741,-40.3089481), type: 'company'} 
  ];

  var contentString = '<div id="content">'+
  '<div id="siteNotice">'+
  '</div>'+
  '<h4 id="firstHeading" class="firstHeading">Igreja Nossa Senhora</h4>'+
  '<div id="bodyContent">'+
  '<p> Aceita doação de resíduos de óleo vegetal, móveis e eletrodomésticos que possam ser utilizados.</p>'+
  '<a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">link</a> '+
  '</div>'+
  '</div>';*/    

  var map = new google.maps.Map(document.getElementById("map"), mapOptions);

  // Create markers.
  entitiesToShow.forEach(function(entity) {
    var marker = new google.maps.Marker({
      position: entity.position,
      icon: icons[entity.type].icon,
      map: map
    });
    var infowindow = new google.maps.InfoWindow({
      content: entity.text,
      maxWidth: 200
    });
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
  });
  
  var marker = new google.maps.Marker({
    position: myLatlng,
    title: "Você está aqui!"
  });

  // To add the marker to the map, call setMap();
  marker.setMap(map);
};

function filterMap() {
  var filter, a;
  input = document.getElementById("filterInput");
  filter = input.value.toUpperCase();
  var cardmap = document.getElementById("card-map");
  var notfound = document.getElementById("not-found");

  if (filter == "") {
    entitiesToShow = entities;
    cardmap.style.display = "block";
    notfound.style.display = "none";
    initGoogleMaps(latlng);
  }

  entitiesToShow = [];
  for (i = 0; i < entities.length; i++) {
      a = entities[i].accept;
      if (a.toUpperCase().indexOf(filter) > -1) {
        entitiesToShow.push(entities[i])
      } 
  }
  
  if (entitiesToShow.length == 0) {
    cardmap.style.display = "none";
    notfound.style.display = "block";
  } else {
    cardmap.style.display = "block";
    notfound.style.display = "none";
    initGoogleMaps(latlng);
  }
};